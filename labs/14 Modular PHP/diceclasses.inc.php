<?php

// Added biased dice
class Dice {
    private  $faces;
    private  $freqs = array();
    private $biased;
    
    // Constructor
    public function __construct($faces) {
        $this->faces = $faces;
        $this->biased = $_GET["biased"];
    }
    
    public function cast() {
        if($this->biased){
            if(rand(1, 10) <= $this->biased*10) $res = $this->faces;
            else $res = rand(1, $this->faces-1);
        }
        else $res = rand(1,$this->faces);
        $this->freqs[$res]++;
        return $res;
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
    public function getAvg(){
        $times = 0;
        $total = 0;
        
        for($i=1; $i<=$this->faces; $i++){
            $times += $this->getFreq($i);
            $total += $i*$this->getFreq($i);
        }
        return $total/$times;
    }
}

// New class PhysicalDice
class PhysicalDice extends Dice {
    private $material;
    
    public function __construct($faces, $material){
        parent::__construct($faces);
        $this->material = $material;
    }
    
    public function getMat(){
        return $this->material;
    }
}

?>