<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$material = $_GET["mat"];

$results = array();

// make dice
if($material) $dice = new PhysicalDice($faces, $material);
else $dice = new Dice($faces);
for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast();
    $results[] = array('id' => strval($i), 'res' => strval($res));
}

$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
}

if($material == null) $material = "This is a regular dice";
else $material = $dice->getMat();

echo json_encode(array('faces'=>$faces,'results'=>$results,'frequencies'=>$freqs, 'material'=>$material, 'average'=>$dice->getAvg()));

?>