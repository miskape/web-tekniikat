var $ = (function () {
    
  // Keep this variable private inside this closure scope
  var something = [11, 22, 33, 44, 55];

  // Expose these functions via an interface while hiding
  // the implementation of the module within the function() block
  return {
    id: function(elementId) {
      var element = document.getElementById(elementId);
      return element;
    }
  }
})();
