
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

/*var list = document.createElement('ol');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('li');
	item.innerHTML = books[i].title+" "+books[i].year;
	list.appendChild(item);
}
document.body.appendChild(list);
*/
window.onload = function addRowHandlers() {
    var table = document.getElementById("table");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) {
                return function() { 
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
										heading.innerHTML = id;
                                 };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
};
var heading = document.createElement('h1');
document.body.appendChild(heading);
var table = document.createElement('table');
var title = document.createElement('th');
var year = document.createElement('th');
var tr = document.createElement('tr');
table.setAttribute('id',"table");
table.appendChild(tr);
heading.innerHTML="Title";
tr.appendChild(title);
tr.appendChild(year);
title.innerHTML = "Title";
year.innerHTML = 'Year';

for (var i = 0; i < books.length; i++) {
	var row = document.createElement('tr');
	var item= document.createElement('td');
	var item2= document.createElement('td');
	item.innerHTML = books[i].title;
	row.appendChild(item);
	item2.innerHTML = books[i].year;
	row.appendChild(item2);
	table.appendChild(row);
}
document.body.appendChild(table);